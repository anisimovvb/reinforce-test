import Vue from 'vue'
import App from './App.vue'
import VueQuillEditor from 'vue-quill-editor'

Vue.use(VueQuillEditor)

import 'quill/dist/quill.core.css'
import 'quill/dist/quill.snow.css'
import 'bootstrap/dist/css/bootstrap.min.css'
import '@/assets/css/main.css'

Vue.config.productionTip = false
import {store} from './store'

new Vue({
  store,
  render: h => h(App),
}).$mount('#app')
