import Vue from 'vue';
import Vuex from 'vuex';

Vue.use(Vuex);

const emptyStep = {
  title: "",
  desc: "",
  hours: [ '00:00:00' ]
}
const emptyTask = {
  id: 0,
  checklist: "",
  steps: [
    JSON.parse(JSON.stringify(emptyStep))
  ],
  errors: [''],
  criteria: [''],
}
export const store = new Vuex.Store({
  state: {
    Task: emptyTask
  },
  getters: {
    GET_STEPS: state => {
      return state.Task.steps
    }
  },
  mutations: {
    ADD_STEP: (state) => {
      state.Task.steps.push(JSON.parse(JSON.stringify(emptyStep)))
    },
    REMOVE_STEP: (state, index) => {
      if (state.Task.steps.length>1) {
        state.Task.steps.splice(index, 1)
      }
    },
    CORRECT_STEP: (state, { oldIndex, newIndex}) => {
      if (newIndex < 0 || newIndex >= state.Task.steps.length) {
        return false
      }
      state.Task.steps[newIndex] = state.Task.steps.splice(oldIndex, 1, state.Task.steps[newIndex])[0]
    },
    ADD_HOURS: (state, stepIndex) => {
      state.Task.steps[stepIndex].hours.push('00:00:00')
    },
    ADD_ITEM: (state, type) => {
      state.Task[type].push("")
    }
  },
  actions: {
    ADD_ITEM (state, type) {
      state.commit('ADD_ITEM', type)
    },
    ADD_HOURS (state, stepIndex) {
      state.commit('ADD_HOURS', stepIndex)
    },
    ADD_STEP (state) {
      state.commit('ADD_STEP')
    },
    REMOVE_STEP (state, index) {
        state.commit('REMOVE_STEP', index)
    },
    MOVE_UP_STEP (state, index) {
      state.commit('CORRECT_STEP', { oldIndex: index, newIndex: index - 1})
    },
    MOVE_DOWN_STEP (state, index) {
      state.commit('CORRECT_STEP', { oldIndex: index, newIndex: index + 1})
    }
  }
});